<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see \craft\config\GeneralConfig
 */

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // Control Panel trigger word
        'cpTrigger' => 'braileycms',

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        // Whether to save the project config out to config/project.yaml
        // (see https://docs.craftcms.com/v3/project-config.html)
        'useProjectConfigFile' => false,

        'activateAccountSuccessPath' => "/clients/login/",

        'rememberedUserSessionDuration' => 1800, // 30 minutes

        'userSessionDuration' => 1800, // 30 minutes

        'loginPath' => "/clients/login/",

        'postLogoutRedirect' => "/clients/login/",

        'postLoginRedirect' => "/clients/resources/",

        'autoLoginAfterAccountActivation' => true,

    ],

    // Dev environment settings
    'dev' => [

        // Prevent administrative changes from being made on staging
        'allowAdminChanges' => true,

        'devMode' => true,

        'siteUrl' => "",

        'aliases' => [
            '@baseUrl' => '',
            '@basePath' => ''
        ],
    ],

    // Staging environment settings
    'staging' => [
        
        // Prevent administrative changes from being made on staging
        'allowAdminChanges' => true,

        'devMode' => true,

        'siteUrl' => "https://braileyhicks.own-design-dev.co.uk",

        'aliases' => [
            '@baseUrl' => 'https://braileyhicks.own-design-dev.co.uk',
            '@basePath' => '/home/storm/sites/braileyhicks-own-design-dev-co-u/public'
        ],

    ],

    // Production environment settings
    'production' => [
        // Set this to `false` to prevent administrative changes from being made on production
        'allowAdminChanges' => true,

        'devMode' => false,

        'siteUrl' => "https://braileyhicks.co.uk",

        'aliases' => [
            '@baseUrl' => 'https://braileyhicks.co.uk',
            '@basePath' => '/home/storm/sites/braileyhicks-co-uk/public'
        ],
    ],
];
